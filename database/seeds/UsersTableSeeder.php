<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            [
                'id'    => 1,
                'name'  => 'Base Admin',
                'email' => 'admin@base.digital',
                'password' => bcrypt('admin'),
                'created_at' => Carbon::now()
            ]
        ]);

        $this->command->info('Admin user successfully created');

        factory(User::class, 9)->create();

        $this->command->info('Fake users successfully created!');
    }
}