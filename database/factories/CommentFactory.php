<?php

use Faker\Generator as Faker;
use App\Models\User;
use App\Models\Post;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->realText(100),
        'user_id' => User::all()->random()->id,
        'post_id' => Post::all()->random()->id,
    ];
});
