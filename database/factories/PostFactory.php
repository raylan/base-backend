<?php

use Faker\Generator as Faker;
use App\Models\User;
use Carbon\Carbon;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'content' => $faker->realText(800),
        'user_id' => User::all()->random()->id,
        'published_at' => Carbon::now()->subDays(rand(1,30)),
    ];
});
