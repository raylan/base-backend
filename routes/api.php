<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('auth/login', 'AuthController@login');
Route::group(['middleware' => ['jwt.auth']], function () {
    Route::get('auth/logout', 'AuthController@logout');
    Route::get('posts', 'PostsController@list');
    Route::get('posts/{id}', 'PostsController@show');
    Route::post('posts', 'PostsController@store');
    Route::put('posts/{id}', 'PostsController@update');
    Route::delete('posts/{id}', 'PostsController@destroy');
    Route::get('comments/{id}', 'CommentsController@list');
    Route::post('comments/{id}', 'CommentsController@store');
});