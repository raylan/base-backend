## Base API

#### Prerequisites:

- Git
- Mysql
- Docker
- Dokcker Compose

#### Instructions

- Create a database with the name of your preference

- Open your terminal and type the following commands:

  - git clone https://gitlab.com/raylan/base-backend.git base-api
  - cd base-api
  - sh start.sh

- Type all the data that is requested in the terminal
- Finished!

#### Additional Information

- Use the command "sh refresh.sh" for reset all data in you database
- Use the command "docker-compose down" for turn off the containers
- Use the command "docker-compose up -d" for turn on the containers