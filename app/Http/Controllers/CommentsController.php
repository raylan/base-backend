<?php

namespace App\Http\Controllers;

use App\Services\CommentService;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    protected $commentService;

    public function __construct(CommentService $commentService)
    {
        $this->commentService = $commentService;
    }

    public function list($id)
    {
        return $this->commentService->getAll($id);
    }

    public function store(Request $request, $id)
    {
        return $this->commentService->create($id, $request->all());
    }
}
