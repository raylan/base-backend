<?php

namespace App\Http\Controllers;

use App\Services\PostService;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function list()
    {
        return $this->postService->getAll();
    }

    public function show($id)
    {
        return $this->postService->getById($id);
    }

    public function store(Request $request)
    {
        return $this->postService->create($request->all());
    }

    public function update(Request $request, $id)
    {
        return $this->postService->update($id, $request->all());
    }

    public function destroy($id)
    {
        return $this->postService->delete($id);
    }
}
