<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable;

class User extends Authenticatable implements JWTSubject, AuditableInterface
{
    use Notifiable;
    use Auditable;

    protected $fillable     = ['name', 'email', 'password', 'last_access'];
    protected $hidden       = ['password', 'remember_token',];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
