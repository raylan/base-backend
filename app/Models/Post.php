<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;
use OwenIt\Auditing\Auditable;

class Post extends Model implements AuditableInterface
{
    use SoftDeletes;
    use Auditable;

    protected $dates    = ['deleted_at'];
    protected $fillable = ['title', 'content', 'user_id', 'published_at'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
