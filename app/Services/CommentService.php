<?php

namespace App\Services;

use App\Models\Comment;
use App\Interfaces\CommentInterface;
use Illuminate\Support\Facades\Auth;

class CommentService implements CommentInterface
{
    protected $comment;
    protected $postService;
    protected $errorService;

    function __construct(Comment $comment, PostService $postService, ErrorService $errorService)
    {
        $this->comment = $comment;
        $this->postService = $postService;
        $this->errorService = $errorService;
    }

    public function getAll($post)
    {
        try {

            if (!$this->postService->check($post))
                return $this->errorService->handleWithError('Post not Found.', 404);

            $comments = $this->comment->with('user:id,name')
                ->where('post_id', $post)
                ->select('id', 'content', 'user_id', 'created_at')
                ->get();

            return response()->json([
                'error' => false,
                'comments' => $comments
            ]);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function create($post, array $data)
    {
        try {

            if (!$this->postService->check($post))
                return $this->errorService->handleWithError('Post not Found.', 404);

            $comment    = new $this->comment;

            $comment->content      = $data['content'];
            $comment->post_id      = $post;
            $comment->user()->associate(Auth::user());

            $comment->save();

            return response()->json([
                'error' => false,
                'message' => 'Comment successfully created.',
                'comment' => $comment
            ]);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }
}