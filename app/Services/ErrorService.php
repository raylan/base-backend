<?php

namespace App\Services;

use App\Interfaces\ErrorInterface;

class ErrorService implements ErrorInterface
{
    public function handleWithError($message, $status)
    {
        return response()->json([
            'error' => true,
            'message' => $message
        ], $status);
    }
}