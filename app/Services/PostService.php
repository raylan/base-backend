<?php

namespace App\Services;

use App\Interfaces\PostInterface;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class PostService implements PostInterface
{
    protected $post;
    protected $errorService;

    function __construct(Post $post, ErrorService $errorService)
    {
        $this->post = $post;
        $this->errorService = $errorService;
    }

    public function check($id)
    {
        $user = $this->post->find($id);
        if (!$user) return false;
        return true;
    }

    public function getAll()
    {
        try {

            $posts = $this->post->with('user:id,name')
                ->select('id', 'title', 'content', 'published_at', 'user_id')
                ->get();

            return response()->json([
                'error' => false,
                'posts' => $posts
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function getById($id)
    {
        try {

            if (!$this->check($id))
                return $this->errorService->handleWithError('Post not Found.', 404);

            $post = $this->post->with('user:id,name')
                ->select('id', 'title', 'content', 'published_at', 'user_id')
                ->find($id);

            return response()->json([
                'error' => false,
                'post' => $post
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function create(array $data)
    {
        try {

            $post = new $this->post;
            $post = $this->fillData($post, $data);
            $post->user()->associate(Auth::user());
            $post->save();

            return response()->json([
                'error' => false,
                'message' => 'Post successfully created.',
                'post' => $post
            ], 201);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function update($id, array $data)
    {
        try {

            if (!$this->check($id))
                return $this->errorService->handleWithError('Post not Found.', 404);

            $post = $this->post->select('id', 'title', 'content', 'published_at')->find($id);
            $post = $this->fillData($post, $data);

            $post->save();

            return response()->json([
                'error' => false,
                'message' => 'Post successfully updated.',
                'post' => $post
            ], 201);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function fillData($post, array $data)
    {
        $post->title        = $data['title'];
        $post->content      = $data['content'];
        $post->published_at = $data['published_at'];

        return $post;
    }

    public function delete($id)
    {
        try {

            if (!$this->check($id))
                return $this->errorService->handleWithError('Post not Found.', 404);

            $this->post->find($id)->delete();

            return response()->json([
                'error' => false,
                'message' => 'Post successfully deleted.'
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }
}