<?php

namespace App\Services;

use App\Interfaces\UserInterface;
use App\Models\User;
use Carbon\Carbon;

class UserService implements UserInterface
{
    protected $user;
    protected $errorService;

    function __construct(User $user, ErrorService $errorService)
    {
        $this->user = $user;
        $this->errorService = $errorService;
    }

    public function lastAccess($id): void
    {
        $user = $this->user->find($id);
        $user->last_access = Carbon::now();
        $user->save();
    }
}