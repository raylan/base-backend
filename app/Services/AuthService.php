<?php

namespace App\Services;

use App\Interfaces\AuthInterface;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService implements AuthInterface
{
    protected $errorService;
    protected $userService;

    function __construct(ErrorService $errorService, UserService $userService)
    {
        $this->errorService = $errorService;
        $this->userService  = $userService;
    }

    public function login(array $data)
    {
        try {

            if (!$token = JWTAuth::attempt($data))
                return $this->errorService->handleWithError('Invalid credentials.', 401);

        } catch (JWTException $e) {
            return $this->errorService->handleWithError('Failed to login, please try again.', 500);
        }

        $this->userService->lastAccess(Auth::user()->id);

        return response()->json([
            'error' => false,
            'token'=> $token
        ]);
    }

    public function logout($token)
    {
        try {

            JWTAuth::invalidate($token);

            return response()->json([
                'error' => false,
                'message'=> "You have successfully logged out."
            ]);

        } catch (JWTException $e) {
            return $this->errorService->handleWithError('Failed to logout, please try again.', 500);
        }
    }

}