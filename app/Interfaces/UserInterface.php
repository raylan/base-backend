<?php

namespace App\Interfaces;


interface UserInterface
{
    public function lastAccess($id): void;
}