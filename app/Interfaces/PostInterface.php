<?php

namespace App\Interfaces;


interface PostInterface
{
    public function check($id);
    public function getAll();
    public function getById($id);
    public function create(array $data);
    public function update($post, array $data);
    public function fillData($post, array $data);
    public function delete($id);
}