<?php

namespace App\Interfaces;


interface CommentInterface
{
    public function getAll($post);
    public function create($post, array $data);
}